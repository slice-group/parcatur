require 'test_helper'

class AffiliatesControllerTest < ActionController::TestCase
  setup do
    @affiliate = affiliates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:affiliates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create affiliate" do
    assert_difference('Affiliate.count') do
      post :create, affiliate: { comapny_address: @affiliate.comapny_address, company_email: @affiliate.company_email, company_name: @affiliate.company_name, company_rif: @affiliate.company_rif, company_web: @affiliate.company_web, fax: @affiliate.fax, id_merchant: @affiliate.id_merchant, old_personal_company: @affiliate.old_personal_company, personal_address: @affiliate.personal_address, personal_email: @affiliate.personal_email, personal_fax: @affiliate.personal_fax, personal_mobile: @affiliate.personal_mobile, personal_name: @affiliate.personal_name, personal_phone: @affiliate.personal_phone, personal_profession: @affiliate.personal_profession, personal_rif: @affiliate.personal_rif, personal_web: @affiliate.personal_web, phone: @affiliate.phone, position_personal_company: @affiliate.position_personal_company, president_name: @affiliate.president_name }
    end

    assert_redirected_to affiliate_path(assigns(:affiliate))
  end

  test "should show affiliate" do
    get :show, id: @affiliate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @affiliate
    assert_response :success
  end

  test "should update affiliate" do
    patch :update, id: @affiliate, affiliate: { comapny_address: @affiliate.comapny_address, company_email: @affiliate.company_email, company_name: @affiliate.company_name, company_rif: @affiliate.company_rif, company_web: @affiliate.company_web, fax: @affiliate.fax, id_merchant: @affiliate.id_merchant, old_personal_company: @affiliate.old_personal_company, personal_address: @affiliate.personal_address, personal_email: @affiliate.personal_email, personal_fax: @affiliate.personal_fax, personal_mobile: @affiliate.personal_mobile, personal_name: @affiliate.personal_name, personal_phone: @affiliate.personal_phone, personal_profession: @affiliate.personal_profession, personal_rif: @affiliate.personal_rif, personal_web: @affiliate.personal_web, phone: @affiliate.phone, position_personal_company: @affiliate.position_personal_company, president_name: @affiliate.president_name }
    assert_redirected_to affiliate_path(assigns(:affiliate))
  end

  test "should destroy affiliate" do
    assert_difference('Affiliate.count', -1) do
      delete :destroy, id: @affiliate
    end

    assert_redirected_to affiliates_path
  end
end
