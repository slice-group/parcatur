require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post :create, employee: { academic_level: @employee.academic_level, address: @employee.address, courses_summary: @employee.courses_summary, date_of_birth: @employee.date_of_birth, email: @employee.email, last_names: @employee.last_names, mobile: @employee.mobile, name: @employee.name, performance_area: @employee.performance_area, phone: @employee.phone, profession: @employee.profession, sex: @employee.sex, skills_summary: @employee.skills_summary, summary_experience: @employee.summary_experience }
    end

    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee
    assert_response :success
  end

  test "should update employee" do
    patch :update, id: @employee, employee: { academic_level: @employee.academic_level, address: @employee.address, courses_summary: @employee.courses_summary, date_of_birth: @employee.date_of_birth, email: @employee.email, last_names: @employee.last_names, mobile: @employee.mobile, name: @employee.name, performance_area: @employee.performance_area, phone: @employee.phone, profession: @employee.profession, sex: @employee.sex, skills_summary: @employee.skills_summary, summary_experience: @employee.summary_experience }
    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_redirected_to employees_path
  end
end
