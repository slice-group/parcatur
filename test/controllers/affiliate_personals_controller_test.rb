require 'test_helper'

class AffiliatePersonalsControllerTest < ActionController::TestCase
  setup do
    @affiliate_personal = affiliate_personals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:affiliate_personals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create affiliate_personal" do
    assert_difference('AffiliatePersonal.count') do
      post :create, affiliate_personal: { city: @affiliate_personal.city, email: @affiliate_personal.email, local: @affiliate_personal.local, mobile: @affiliate_personal.mobile, municipality: @affiliate_personal.municipality, name: @affiliate_personal.name, ocupation: @affiliate_personal.ocupation, phone: @affiliate_personal.phone, profession: @affiliate_personal.profession, rif_personal: @affiliate_personal.rif_personal, sector: @affiliate_personal.sector, street: @affiliate_personal.street }
    end

    assert_redirected_to affiliate_personal_path(assigns(:affiliate_personal))
  end

  test "should show affiliate_personal" do
    get :show, id: @affiliate_personal
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @affiliate_personal
    assert_response :success
  end

  test "should update affiliate_personal" do
    patch :update, id: @affiliate_personal, affiliate_personal: { city: @affiliate_personal.city, email: @affiliate_personal.email, local: @affiliate_personal.local, mobile: @affiliate_personal.mobile, municipality: @affiliate_personal.municipality, name: @affiliate_personal.name, ocupation: @affiliate_personal.ocupation, phone: @affiliate_personal.phone, profession: @affiliate_personal.profession, rif_personal: @affiliate_personal.rif_personal, sector: @affiliate_personal.sector, street: @affiliate_personal.street }
    assert_redirected_to affiliate_personal_path(assigns(:affiliate_personal))
  end

  test "should destroy affiliate_personal" do
    assert_difference('AffiliatePersonal.count', -1) do
      delete :destroy, id: @affiliate_personal
    end

    assert_redirected_to affiliate_personals_path
  end
end
