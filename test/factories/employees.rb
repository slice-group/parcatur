# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :employee do
    name "MyString"
    last_names "MyString"
    sex "MyString"
    email "MyString"
    date_of_birth "MyString"
    phone "MyString"
    mobile "MyString"
    address "MyText"
    academic_level "MyString"
    performance_area "MyText"
    profession "MyString"
    skills_summary "MyText"
    courses_summary "MyText"
    summary_experience "MyText"
  end
end
