# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :affiliate_personal do
    rif_personal "MyString"
    name "MyString"
    profession "MyString"
    ocupation "MyString"
    phone "MyString"
    mobile "MyString"
    email "MyString"
    municipality "MyString"
    city "MyString"
    sector "MyString"
    street "MyString"
    local "MyString"
  end
end
