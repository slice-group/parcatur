# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :affiliate do
    company_name ""
    president_name ""
    company_rif ""
    id_merchant ""
    comapny_address ""
    phone ""
    fax ""
    company_email ""
    company_web ""
    personal_name ""
    personal_rif ""
    personal_profession ""
    position_personal_company ""
    old_personal_company ""
    personal_phone ""
    personal_fax ""
    personal_mobile ""
    personal_address ""
    personal_email ""
    personal_web "MyString"
  end
end
