Blogadding::Application.routes.draw do
  
  resources :affiliate_personals

  resources :employees

  resources :affiliates

  mount Ckeditor::Engine => '/ckeditor'
  root :to => "home#index"
  devise_for :users, :skip => [:registrations] 
  resources :users
  resources :admin, only: [:index]
  resources :blog, only: [:index, :show], as: 'blog', controller: 'blogadd'

  get "home/about", path: "about"
  get "home/affiliate", path: "affiliate"
  get "home/news", path: "news"
  get "home/links", path: "links"
  get "home/contact_us", path: "contact_us"
  get "home/news/:id/show",to: "home#show", as: "news_show"
  post "home/send_contact_message"
  get "home/search_affiliate"
  get "home/search_affiliate_professional"

  #pdfs
  get "pdfs/registration_sheet/:id", to: "pdfs#registration_sheet", as:"pdf_registration_sheet"
  get "pdfs/registration_professional/:id", to: "pdfs#registration_professional", as:"pdf_registration_professional"
  #get "pdfs/download/:document", to: "pdfs#download", as:"pdf_download"


  scope :admin do
  	resources :posts
  	resources :categories
  end

  match "/posts/add_new_comment" => "posts#add_new_comment", :as => "add_new_comment_to_posts", :via => [:post]
 
end