require "#{Rails.root}/lib/engine.rb"

Blog.setup do |config|
	
  config.commentable = false #true = posts comentables | false = posts no commentables
  config.site_name = "Parcatur" #Nombre del sitio web
  config.votable = true #true = posts votables | false = posts no votables
  config.comments_facebook = true #true = posts comentables  facebook| false = posts no commentables facebook
  config.widget_twitter_id = '473556104295620608' #add id widget timeline twitter
  
end