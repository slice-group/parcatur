# config/initializers/pdfkit.rb
PDFKit.configure do |config|
  config.default_options = {
    :encoding      => 'UTF-8',
    :page_size     => 'Letter',
    :margin_top    => '0.3in',
    :margin_right  => '0.4in',
    :margin_bottom => '0.4in',
    :margin_left   => '0.4in'
  }
  # Use only if your external hostname is unavailable on the server.
  config.root_url = "http://parcatur.org"
  config.verbose = false
end
