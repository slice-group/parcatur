class Affiliate < ActiveRecord::Base

	validates_presence_of :company_name, :president_name, :company_rif, :id_merchant, :phone, :company_email, :personal_name, :personal_rif, :personal_phone, :personal_mobile, :personal_address, :personal_email, :company_category, :company_municipality, :company_city, :company_sector, :company_street, :company_local
	validates_numericality_of :company_rif, :personal_rif, :phone, :personal_phone, :personal_mobile, :only_integer => true, :allow_nil => true
	validates :company_email, :personal_email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "formato de email no valido" }
	validates :company_rif, :length => { minimum: 8, maximum: 10 }, :uniqueness => true
	validates :phone, :personal_phone, :personal_mobile, :length => { minimum: 7, maximum: 15 }
	validates :personal_address, :length => { minimum: 10, maximum: 100 }
	validates :company_name, :uniqueness => true

	
	def self.search(search)
    find(:all, :conditions => ['company_rif iLIKE ?', ["%#{search}%"]].flatten)
  end
end
