class Post < ActiveRecord::Base
  acts_as_commentable
	belongs_to :user
  belongs_to :category
  mount_uploader :image, ImageUploader

  validates_presence_of :title, :content, :user_id, :category_id, :image
  validate :picture_size_validation, :if => "image?"  
	#search
  def self.search(search)
    if search
      @post = find(:all, :conditions => ['public = ? AND title iLIKE ? OR content iLIKE ?', true, ["%#{search}%"]*2].flatten)
    else
      @post = where('public = true').order('id DESC')
    end
  end

  def picture_size_validation
    errors[:image] << "Imagén no puede pesar más de 1 MB" if self.image.size > 1.megabytes
  end

end
