class AffiliatePersonal < ActiveRecord::Base

	validates_presence_of :name, :rif_personal, :profession, :ocupation, :phone, :mobile, :email, :municipality, :city, :street, :local, :sector
	validates_numericality_of :phone, :mobile, :rif_personal, :only_integer => true
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "formato de email no valido" }
	validates :rif_personal, :length => { minimum: 8, maximum: 10 }, :uniqueness => true
	validates :phone, :mobile, :length => { minimum: 7, maximum: 15 }

	def self.search(search)
    where "rif_personal = '#{search}'"
  end
end
