class Employee < ActiveRecord::Base
	validates_uniqueness_of :rif_personal
	validates_presence_of :rif_personal, :name, :last_names, :sex, :email, :date_of_birth, :phone, :address, :academic_level, :profession, :performance_area, :skills_summary, :courses_summary, :summary_experience
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create, :message => "formato de email no valido" }
	validates :phone, :mobile, :length => { minimum: 7, maximum: 15 }
	validates :rif_personal, :length => { minimum: 8, maximum: 9 }
	validates_numericality_of :phone, :mobile, :rif_personal
end
