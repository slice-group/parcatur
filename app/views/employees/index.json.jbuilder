json.array!(@employees) do |employee|
  json.extract! employee, :id, :name, :last_names, :sex, :email, :date_of_birth, :phone, :mobile, :address, :academic_level, :performance_area, :profession, :skills_summary, :courses_summary, :summary_experience
  json.url employee_url(employee, format: :json)
end
