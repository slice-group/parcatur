json.array!(@affiliate_personals) do |affiliate_personal|
  json.extract! affiliate_personal, :id, :rif_personal, :name, :profession, :ocupation, :phone, :mobile, :email, :municipality, :city, :sector, :street, :local
  json.url affiliate_personal_url(affiliate_personal, format: :json)
end
