json.array!(@affiliates) do |affiliate|
  json.extract! affiliate, :id, :company_name, :president_name, :company_rif, :id_merchant, :comapny_address, :phone, :fax, :company_email, :company_web, :personal_name, :personal_rif, :personal_profession, :position_personal_company, :old_personal_company, :personal_phone, :personal_fax, :personal_mobile, :personal_address, :personal_email, :personal_web
  json.url affiliate_url(affiliate, format: :json)
end
