class NotifyClient < ActionMailer::Base
  default from: "info@parcatur.org"

  def notifyclient(affiliate)
    @affiliate = affiliate
    mail(to: @affiliate.company_email, subject: @affiliate.company_name+' Bienvenido a PARCATUR')
  end

  def notifyparcatur(affiliate)
  	@affiliate = affiliate
    mail(to: 'info@parcatur.org', subject: @affiliate.company_name+' ha realizado una solicitud de afiliación')
  end

  def notifyprofesional(affiliate)
    @affiliate = affiliate
    mail(to: @affiliate.email, subject: @affiliate.name+' Bienvenido a PARCATUR')
  end

  def notifyparcaturprofesional(affiliate)
  	@affiliate = affiliate
    mail(to: 'info@parcatur.org', subject: @affiliate.name+' ha realizado una solicitud de afiliación')
  end
end
