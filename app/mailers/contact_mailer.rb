class ContactMailer < ActionMailer::Base
  default from: "info@parcatur.org"

  def contact(client)
    @name = client[:name]
    @email = client[:email]
    @comment = client[:comment]
    mail(to: "info@parcatur.org", subject: @name+' ha contactado con Parcatur.')
  end
end
