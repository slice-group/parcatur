class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /posts
  # GET /posts.json
  def index
    if current_user.has_role? :admin 
      @posts = Post.all

    else
      @posts = Post.where(user_id: current_user.id)
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    if @post.user_id != current_user.id and current_user.roles.first != "admin" 
      authorize! :index, @user, :message => 'Necesita autorización de administrador para poder ver este post.'
    end
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    if current_user.roles.first.name != "admin"
      if Post.find(params[:id]).user_id != current_user.id #evitar que un usuario no admin pueda evitar otro post que no sea de su propiedad
        raise CanCan::AccessDenied.new("No esta autorizado para editar este post.", :edit, Post)
      end
    end
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end

  #añadir comentario al post
  def add_new_comment
    post = Post.find(params[:id])
    post.comments << Comment.new(comment: params[:comment], user_id: params[:user_id], name: params[:name])
    redirect_to blog_path(post.id, anchor: 'comment-area')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :content, :public, :category_id, :user_id, :image)
    end
end
