class AffiliatesController < ApplicationController
  before_action :set_affiliate, only: [:show, :edit, :update, :destroy]

  # GET /affiliates
  # GET /affiliates.json
  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @affiliates = Affiliate.all
  end

  # GET /affiliates/1
  # GET /affiliates/1.json
  def show
  end

  # GET /affiliates/new
  def new
    @affiliate = Affiliate.new
  end

  # GET /affiliates/1/edit
  def edit
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  end

  # POST /affiliates
  # POST /affiliates.json
  def create
    @affiliate = Affiliate.new(affiliate_params)

    respond_to do |format|
      if @affiliate.save
        NotifyClient.notifyclient(@affiliate).deliver
        NotifyClient.notifyparcatur(@affiliate).deliver
        format.html { redirect_to @affiliate, notice: 'Affiliate was successfully created.' }
        format.json { render action: 'show', status: :created, location: @affiliate }
      else
        format.html { render action: 'new' }
        format.json { render json: @affiliate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affiliates/1
  # PATCH/PUT /affiliates/1.json
  def update
    respond_to do |format|
      if @affiliate.update(affiliate_params)
        format.html { redirect_to @affiliate, notice: 'Affiliate was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @affiliate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /affiliates/1
  # DELETE /affiliates/1.json
  def destroy
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @affiliate.destroy
    respond_to do |format|
      format.html { redirect_to affiliates_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affiliate
      @affiliate = Affiliate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affiliate_params
      params.require(:affiliate).permit(:company_name, :president_name, :company_rif, :rif_type,:id_merchant, :phone, :fax, :company_email, :company_web, :personal_name, :personal_rif, :personal_profession, :position_personal_company, :old_personal_company, :personal_phone, :personal_fax, :personal_mobile, :personal_address, :personal_email, :personal_web, :company_rif, :company_category, :company_municipality, :company_city, :company_sector, :company_street, :company_local)
    end
end
