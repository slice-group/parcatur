class HomeController < ApplicationController
	def about		
	end

	def affiliate		
	end

	def news
  	if params[:search]
			@posts = Post.search(params[:search])
			if @posts.class == Array
			  @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(3) 
			else
			  @posts = @posts.page(params[:page]).per(3)
			end
		else 
			 @posts = Post.where('public = true').order('id DESC')
			 @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(3) 
    end
    @recents_posts = Post.where('public = true').first(5).reverse
	end

	def links
	end

	def contact_us
	end

	def show
		@post = Post.find(params[:id])
		@recents_posts = Post.first(5).reverse
	end

	def send_contact_message
		if verify_recaptcha(attribute: "contact", message: "Oh! It's error with reCAPTCHA!")
	  	ContactMailer.contact(params).deliver
	  	redirect_to home_contact_us_path,  flash: { notice: params[:name]+', ¡Tu mensaje ha sido en enviado!' }
	  else
	  	redirect_to home_contact_us_path
	  end
	end

	def search_affiliate
    unless params[:search].blank?
      @affiliate = Affiliate.search(params[:search]).first
      if @affiliate.blank?
        redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
      elsif params[:rif_letter] == @affiliate.rif_type
        redirect_to affiliate_path(@affiliate.id)
      else
      	redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
      end
    else
    	redirect_to new_affiliate_path, flash: { danger: 'No se encuenta registrado.' }
    end
  end

  def search_affiliate_professional
    unless params[:search].blank?
      @affiliate = AffiliatePersonal.search(params[:search]).first
      if @affiliate.blank?
        redirect_to new_affiliate_personal_path, flash: { danger: 'No se encuenta registrado.' }
      else
        redirect_to affiliate_personal_path(@affiliate.id)
      end
    else
    	redirect_to new_affiliate_personal_path, flash: { danger: 'No se encuenta registrado.' }
    end
  end
end