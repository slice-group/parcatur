class BlogaddController < ApplicationController
  def index
  	if params[:search]
			@posts = Post.search(params[:search])
			if @posts.class == Array
			  @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(6) 
			else
			  @posts = @posts.page(params[:page]).per(6)
			end
		else 
			 @posts = Post.all
			 @posts = Kaminari.paginate_array(@posts).page(params[:page]).per(6) 
    end
  end

  def show
  	@post = Post.find(params[:id])
  end
end
