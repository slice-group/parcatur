class AffiliatePersonalsController < ApplicationController
  before_action :set_affiliate_personal, only: [:show, :edit, :update, :destroy]

  # GET /affiliate_personals
  # GET /affiliate_personals.json
  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @affiliate_personals = AffiliatePersonal.all
  end

  # GET /affiliate_personals/1
  # GET /affiliate_personals/1.json
  def show
  end

  # GET /affiliate_personals/new
  def new
    @affiliate_personal = AffiliatePersonal.new
  end

  # GET /affiliate_personals/1/edit
  def edit
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
  end

  # POST /affiliate_personals
  # POST /affiliate_personals.json
  def create
    @affiliate_personal = AffiliatePersonal.new(affiliate_personal_params)

    respond_to do |format|
      if @affiliate_personal.save
        NotifyClient.notifyprofesional(@affiliate_personal).deliver
        NotifyClient.notifyparcaturprofesional(@affiliate_personal).deliver
        format.html { redirect_to @affiliate_personal, notice: 'Affiliate personal was successfully created.' }
        format.json { render action: 'show', status: :created, location: @affiliate_personal }
      else
        format.html { render action: 'new' }
        format.json { render json: @affiliate_personal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affiliate_personals/1
  # PATCH/PUT /affiliate_personals/1.json
  def update
    respond_to do |format|
      if @affiliate_personal.update(affiliate_personal_params)
        format.html { redirect_to @affiliate_personal, notice: 'Affiliate personal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @affiliate_personal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /affiliate_personals/1
  # DELETE /affiliate_personals/1.json
  def destroy
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @affiliate_personal.destroy
    respond_to do |format|
      format.html { redirect_to affiliate_personals_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affiliate_personal
      @affiliate_personal = AffiliatePersonal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affiliate_personal_params
      params.require(:affiliate_personal).permit(:rif_personal, :name, :profession, :ocupation, :phone, :mobile, :email, :municipality, :city, :sector, :street, :local)
    end
end
