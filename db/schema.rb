# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140821011345) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "affiliate_personals", force: true do |t|
    t.string   "rif_personal"
    t.string   "name"
    t.string   "profession"
    t.string   "ocupation"
    t.string   "phone"
    t.string   "mobile"
    t.string   "email"
    t.string   "municipality"
    t.string   "city"
    t.string   "sector"
    t.string   "street"
    t.string   "local"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "affiliates", force: true do |t|
    t.string   "company_name"
    t.string   "president_name"
    t.string   "company_rif"
    t.string   "id_merchant"
    t.string   "phone"
    t.string   "fax"
    t.string   "company_email"
    t.string   "company_web"
    t.string   "personal_name"
    t.string   "personal_rif"
    t.string   "personal_profession"
    t.string   "position_personal_company"
    t.string   "old_personal_company"
    t.string   "personal_phone"
    t.string   "personal_fax"
    t.string   "personal_mobile"
    t.text     "personal_address"
    t.string   "personal_email"
    t.string   "personal_web"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rif_type"
    t.string   "category"
    t.string   "company_category"
    t.string   "company_municipality"
    t.string   "company_city"
    t.string   "company_sector"
    t.string   "company_street"
    t.string   "company_local"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: true do |t|
    t.string   "title",            limit: 50, default: ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.string   "role",                        default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "employees", force: true do |t|
    t.string   "rif_personal"
    t.string   "name"
    t.string   "last_names"
    t.string   "sex"
    t.string   "email"
    t.string   "date_of_birth"
    t.string   "phone"
    t.string   "mobile"
    t.text     "address"
    t.string   "academic_level"
    t.text     "performance_area"
    t.string   "profession"
    t.text     "skills_summary"
    t.text     "courses_summary"
    t.text     "summary_experience"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "paquetes", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.string   "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "public"
    t.integer  "category_id"
    t.integer  "user_id"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "image"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
