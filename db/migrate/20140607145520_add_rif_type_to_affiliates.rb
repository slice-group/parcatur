class AddRifTypeToAffiliates < ActiveRecord::Migration
  def change
    add_column :affiliates, :rif_type, :string
  end
end
