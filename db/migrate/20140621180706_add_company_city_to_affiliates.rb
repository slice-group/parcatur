class AddCompanyCityToAffiliates < ActiveRecord::Migration
  def change
    add_column :affiliates, :company_city, :string
  end
end
