class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :rif_personal
      t.string :name
      t.string :last_names
      t.string :sex
      t.string :email
      t.string :date_of_birth
      t.string :phone
      t.string :mobile
      t.text :address
      t.string :academic_level
      t.text :performance_area
      t.string :profession
      t.text :skills_summary
      t.text :courses_summary
      t.text :summary_experience

      t.timestamps
    end
  end
end
