class AddColumnsToAffiliates < ActiveRecord::Migration
  def change
    add_column :affiliates, :company_sector, :string
    add_column :affiliates, :company_street, :string
    add_column :affiliates, :company_local, :string
  end
end
