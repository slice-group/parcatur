class AddCompanyMunicipalityToAffiliates < ActiveRecord::Migration
  def change
    add_column :affiliates, :company_municipality, :string
  end
end
