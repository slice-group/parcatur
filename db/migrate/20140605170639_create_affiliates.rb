class CreateAffiliates < ActiveRecord::Migration
  def change
    create_table :affiliates do |t|
      t.string :company_name
      t.string :president_name
      t.string :company_rif
      t.string :id_merchant
      t.text :comapny_address
      t.string :phone
      t.string :fax
      t.string :company_email
      t.string :company_web
      t.string :personal_name
      t.string :personal_rif
      t.string :personal_profession
      t.string :position_personal_company
      t.string :old_personal_company
      t.string :personal_phone
      t.string :personal_fax
      t.string :personal_mobile
      t.text :personal_address
      t.string :personal_email
      t.string :personal_web

      t.timestamps
    end
  end
end
