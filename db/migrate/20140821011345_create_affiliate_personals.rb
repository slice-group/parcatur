class CreateAffiliatePersonals < ActiveRecord::Migration
  def change
    create_table :affiliate_personals do |t|
      t.string :rif_personal
      t.string :name
      t.string :profession
      t.string :ocupation
      t.string :phone
      t.string :mobile
      t.string :email
      t.string :municipality
      t.string :city
      t.string :sector
      t.string :street
      t.string :local

      t.timestamps
    end
  end
end
