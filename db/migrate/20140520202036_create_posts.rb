class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.boolean :public
      t.integer :category_id
      t.integer :user_id
      t.string :image

      t.timestamps
    end
  end
end
