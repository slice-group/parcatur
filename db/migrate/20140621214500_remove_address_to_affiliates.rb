class RemoveAddressToAffiliates < ActiveRecord::Migration
  def change
    remove_column :affiliates, :comapny_address
  end
end
