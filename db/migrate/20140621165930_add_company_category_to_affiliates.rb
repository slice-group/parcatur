class AddCompanyCategoryToAffiliates < ActiveRecord::Migration
  def change
    add_column :affiliates, :company_category, :string
  end
end
